//
//  CipherUtils.swift
//  Matasano Challenge
//
//  Created by Keith Schaefer on 5/10/16.
//  Copyright © 2016 Chick-fil-A, Inc. All rights reserved.
//

import Foundation

func bytesToUTF8String(bytes: [UInt8]) -> String? {
    return NSString(bytes: bytes, length: bytes.count, encoding: NSUTF8StringEncoding) as? String
}

extension String {
    func hexStringToUTF8String() -> String? {
        let a = self.hexStringToBytes()
        return NSString(bytes: a, length: a.count, encoding: NSUTF8StringEncoding) as? String
    }
    
    func base64StringToUTF8String() -> String {
        let decodedData = NSData(base64EncodedString: self, options:NSDataBase64DecodingOptions(rawValue: 0))
        return NSString(data: decodedData!, encoding: NSUTF8StringEncoding) as! String
    }
    
    func hexStringToBytes() -> [UInt8] {
        let chars = Array(self.characters)
        
        let numbers = 0.stride(to: chars.count, by: 2).map {
            UInt8(String(chars[$0 ..< $0+2]), radix: 16) ?? 0
        }
        
        return numbers
    }
    
    func xorWithRepeatingKey(key: [String]) -> String {
        let length = self.characters.count
        var repeatedKey = ""
        for i in 0 ..< length {
            repeatedKey += key[i % 3]
        }
        let repeatedKeyBytes = repeatedKey.toBytes()
        
        return self.toBytes().enumerate().map{$0.element ^ repeatedKeyBytes[$0.index]}.bytesToHexString()
    }
    
    func xorWithHexString(buffer: String) -> String {
        guard self.characters.count == buffer.characters.count
            else { return "" }
        
        let byteBuffer1 = self.hexStringToBytes()
        var byteBuffer2 = buffer.hexStringToBytes()

        let newBuffer = byteBuffer1.enumerate().map{$0.element ^ byteBuffer2[$0.index]}
        
        return newBuffer.bytesToHexString()
    }
    
    func toBytes() -> [Byte] {
        var byteArray = [Byte]()
        for char in self.utf8{
            byteArray += [char]
        }
        return byteArray
    }
    
    func lookupScore() -> Double {
        var score = 0.0
        for char in self.characters {
            score += char.lookupScore()
        }
        return score
    }
    
    func evaluateSingleDigitCipher(cipherDigit: UInt8) -> String? {
        let bytes = self.hexStringToBytes()
        var decodedBuffer = [UInt8]()
        
        for byte in bytes {
            decodedBuffer.append(byte ^ cipherDigit)
        }
        
        var answer: String?
        answer = bytesToUTF8String(decodedBuffer)
        
        //    print("Answer: \(answer)")
        return answer
    }
    
    func calculateWinningSingleDigitCipher() -> (Double, Byte) {
        var topScore = 0.0
        var winningIndex : Byte =  0
        for i in 0 ... 255 {
//            print("test cipher \(i)")
            let test = self.evaluateSingleDigitCipher(UInt8(i))
            let score = test?.lookupScore()
            
//            if score == nil {
//                return (-1000, 0)
//            }
//            
            if score != nil && score > topScore {
                topScore = score!
                winningIndex = Byte(i)
            }
        }
        
        return (topScore, winningIndex)
    }
}

let scores: [Character : Double] = [
    "a" : 0.0651738,
    "b" : 0.0124248,
    "c" : 0.0217339,
    "d" : 0.0349835,
    "e" : 0.1041442,
    "f" : 0.0197881,
    "g" : 0.0158610,
    "h" : 0.0492888,
    "i" : 0.0558094,
    "j" : 0.0009033,
    "k" : 0.0050529,
    "l" : 0.0331490,
    "m" : 0.0202124,
    "n" : 0.0564513,
    "o" : 0.0596302,
    "p" : 0.0137645,
    "q" : 0.0008606,
    "r" : 0.0497563,
    "s" : 0.0515760,
    "t" : 0.0729357,
    "u" : 0.0225134,
    "v" : 0.0082903,
    "w" : 0.0171272,
    "x" : 0.0013692,
    "y" : 0.0145984,
    "z" : 0.0007836,
    " " : 0.1918182
]

extension Character {
    func lookupScore() -> Double  {
        let score = scores[self]
        return score ?? 0.0
    }
}

//Typed array extension
extension SequenceType where Generator.Element == Byte {
    func bytesToBase64String() -> String {
        return NSData(bytes: self as! [UInt8], length: (self as! [UInt8]).count).base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
    }
    
    func bytesToHexString() -> String {
        return self.map {
            var s = String($0, radix: 16)
            if s.characters.count == 1 {
                s = "0" + s
            }
            return s
        }.reduce("", combine: +)
    }
}