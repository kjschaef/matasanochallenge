//
//  SetOne.swift
//  Matasano Challenge
//
//  Created by Keith Schaefer on 6/27/16.
//  Copyright © 2016 Chick-fil-A, Inc. All rights reserved.
//

import Foundation

class SetOne {
    static func challengeOne() {
        let hexInput = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
        
        let bytes = hexInput.hexStringToBytes()
        let base64String = bytes.bytesToBase64String()
        
        let base64Answer = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
        print("Challenge 1 solved: \(base64String == base64Answer)")
        
        //Extra credit: print out the input string
        print(base64Answer.base64StringToUTF8String())
    }
 
    static func challengeTwo() {
        let xorInput1 = "1c0111001f010100061a024b53535009181c"
        let xorInput2 = "686974207468652062756c6c277320657965"
        var xorString = ""
        
        xorString = xorInput1.xorWithHexString(xorInput2)
        let xorAnswer = "746865206b696420646f6e277420706c6179"
        print("Challenge 2 solved: \(xorString == xorAnswer)")
        
        //Extra credit, print out the input and answer strings decoded
        print(xorInput2.hexStringToUTF8String()!)
        print(xorAnswer.hexStringToUTF8String()!)
    }
    
    //Single-byte XOR Cipher
    static func challengeThree() {
        let cipheredHexInput = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
        
        let (score, cipher) = cipheredHexInput.calculateWinningSingleDigitCipher()
        
        let chal3Answer = cipheredHexInput.evaluateSingleDigitCipher(UInt8(cipher))!
        
        print(chal3Answer)
        print("Cipher digit used: \(cipher)  score: \(score)")
        print("Challenge 3 solved: \(chal3Answer == "Cooking MC's like a pound of bacon")")
    }
    
    //Single-byte XOR Cipher for file
    static func challengeFour() {
        let lines = Constants.setOneChallengeFourData.componentsSeparatedByString("/n")
        
        var answers = [Int: (Double, Byte)]()
        var i = 0
        for line in lines {
            let (score, answer) = line.calculateWinningSingleDigitCipher()
            answers[i] = (score, answer)
//            if answer > 0 {
//                print("line: \(i)  Cipher: \(answer)   score: \(score)")
//            }
            i += 1
        }
        
        var winningCipher :(Double,Byte) = (0,0)
        var winningKey = 0
        i = 0
        for (key, value) in answers {
            if value.0 > winningCipher.0 {
                winningCipher = value
                winningKey = key
            }
            i += 1
        }
        
        print("Challenge 4 answer: \(winningCipher.1). Line: \(lines[winningKey])  score: \(winningCipher.0)")
        let decodedAnswer = lines[winningKey].evaluateSingleDigitCipher(winningCipher.1)!
        print(decodedAnswer)
        
        print("Challenge 4 solved: \(decodedAnswer == "Now that the party is jumping\n")")
    }
    
    //Repeating key XOR
    static func challengeFive() {
        let inputLine1 = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"
        let keys = ["I", "C", "E"]
        
        let encryptedLine1 = inputLine1.xorWithRepeatingKey(keys)

        let answerLine1 = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"
            "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"
        print("Challenge 5 solved: \(answerLine1 == encryptedLine1)")// && answerLine2 == encryptedLine2)")
        
        //Extra credit, print out the input and answer strings decoded
        print(encryptedLine1)
    }
    
    //Hamming distance
    static func challengeSix() {
        //for each byte, xor it with cipher, then count the bits set
        //  numberOfBitsSet(b: Byte) : Int = (0 to 7).map((i : Int) => (b >>> i) & 1).sum - http://www.tautvidas.com/blog/2013/07/compute-hamming-distance-of-byte-arrays/
        //  OR loop through bits and xor each bit, adding the result to the total distance - http://stackoverflow.com/questions/12741613/finding-hamming-distance-of-code
        //See reference for bitwise operators in swift - https://www.uraimo.com/2016/02/05/Dealing-With-Bit-Sets-In-Swift/
    }
}
